<?php
/**
 * The main admin class, holds everything BPPS theme can customize,
 * Initialized right after declaration
 */
class BPAP_Admin_Tabs {

    // Declare Tabs
    private $dash_settings_key      = 'dashboard';
    private $forums_settings_key    = 'f_topics';
    private $info_settings_key      = 'author';
    private $settings_settings_key  = 'settings';
    private $notices_settings_key   = 'notices';
    private $unban_settings_key     = 'unban';
    private $calendar_settings_key  = 'calendar';

    // general values
    private $bpap_options_key       = 'bpap_admin';
    private $bpap_settings_tabs     = array();

    // all plugin settings
    private $bpap                   = array();

    /**
     * Register stat page & tabs
     */
    function __construct() {
        // handle all the saving
        add_action('admin_init', array(&$this, 'save_data'));

        // the first one
        add_action('admin_init', array(&$this, 'register_dash_page'));
        // very important calendar
        add_action('admin_init', array(&$this, 'register_calendar_page'));
        // all option are here
        if( bp_is_active('forums') && bp_forums_is_installed_correctly() ) {
            add_action('admin_init', array(
                    &$this,
                    'register_forums_page'
                )
            );
        }
        if( bp_is_active('messages') ) {
            add_action('admin_init', array(
                    &$this,
                    'register_notify_page'
                )
            );
        }
        if( bp_is_active('groups') ) {
            add_action('admin_init', array(
                    &$this,
                    'register_unban_page'
                )
            );
        }
        // add some 3rd party items
        do_action('bpap_admin_add_tab', $this);
        // last ones
        add_action('admin_init', array(&$this, 'register_settings_page'));
        add_action('admin_init', array(&$this, 'register_info_page'));

        if(is_multisite()){
            add_action('network_admin_menu', array(&$this, 'add_admin_menu'));
        }else{
            add_action('admin_menu', array(&$this, 'add_admin_menu'));
        }

        add_action('bpap_admin_footer', array(&$this, 'bpap_footer'), 99);

        // get all settings
        $this->bpap = get_option('bpap');
    }

    /**
     * Here all the save process will take place
     * @return void
     */
    function save_data(){
        if(!isset($_POST['bpap']) || !isset($_POST['bpap']['action']))
            return;

        // make the var shorter :)
        $post = $_POST['bpap'];
        $link = $_POST['_wp_http_referer'];

        $changed = false;

        // Save Notifications
        if(isset($post['action']['settings']['notify'])){
            // Email notifications
            $this->bpap['notify']['email'] = array();
            if(isset($post['notify']['email'])){
                $changed[] = 'email';
                $this->bpap['notify']['email'] = $post['notify']['email'];
            }
        }

        // Save Emails content
        if(isset($post['action']['settings']['emails'])){
            $changed[] = 'emails';
            $this->bpap['emails']['f_topics']['subject'] = strip_tags($post['emails']['f_topics']['subject']);
            $this->bpap['emails']['f_topics']['content'] = nl2br(htmlspecialchars($post['emails']['f_topics']['content']));
        }

        // Deactivate options
        if(isset($post['action']['settings']['deactivate'])){
            $changed[] = 'deactivate';
            $this->bpap['deactivate'] = $post['deactivate'];
        }

        // Cron precision
        if(isset($post['action']['settings']['cron'])){
            $changed[] = 'cron';
            $this->bpap['cron'] = $post['cron'];
        }

        do_action('bpap_admin_save_settings', $this->bpap, $post);

        // Save settings
        if($changed){
            if(update_option('bpap', $this->bpap)){
                $link = add_query_arg(array(
                                        'message' => 'settings_saved'
                                    ), $_POST['_wp_http_referer']);
            }else{
                $link = add_query_arg(array(
                                        'message' => 'settings_saved_error'
                                    ), $_POST['_wp_http_referer']);
            }
        }

        // Topic Create / Update
        if(isset($post['action']['topic_create']) ||
           isset($post['action']['topic_update'])
        ){
            // server side prevention of required empty fields
            if(
               empty($post['title']) ||
               empty($post['content']) ||
               empty($post['author_id']) ||
               empty($post['group_id']) ||
               empty($post['scheduled_date'])
            ){
                $link = add_query_arg(array(
                                        'message' => 'error_req'
                                    ), $_POST['_wp_http_referer']);
                wp_redirect($link);
                die;
            }

            // need to create a link to the post
            $id = $this->forums_topic_create_update($post);
            if($id){ // success
                // redirect to edit page
                $link = add_query_arg(array(
                                        'message' => 'topic_saved',
                                        'action'  => 'edit',
                                        'id'      => $id
                                    ), $_POST['_wp_http_referer']);
            }else{ // error on saving
                // redirect to itself
                $link = add_query_arg(array(
                                        'message' => 'topic_error'
                                    ), $_POST['_wp_http_referer']);
            }
        }

        // Notice Create / Update
        if(isset($post['action']['notice_create']) ||
           isset($post['action']['notice_update'])
        ){
            // server side prevention of required empty fields
            if(
               empty($post['subject']) ||
               empty($post['message']) ||
               empty($post['author_id']) ||
               empty($post['scheduled_date'])
            ){
                $link = add_query_arg(array(
                                        'message' => 'error_req'
                                    ), $_POST['_wp_http_referer']);
                wp_redirect($link);
                die;
            }

            // need to create a link to the post
            $id = $this->notices_create_update($post);
            if($id){ // success
                // redirect to edit page
                $link = add_query_arg(array(
                                        'message' => 'notice_saved',
                                        'action'  => 'edit',
                                        'id'      => $id
                                    ), $_POST['_wp_http_referer']);
            }else{ // error on saving
                // redirect to itself
                $link = add_query_arg(array(
                                        'message' => 'notice_error'
                                    ), $_POST['_wp_http_referer']);
            }
        }

        // Unban Action Create / Update
        if(isset($post['action']['unban_create']) ||
           isset($post['action']['unban_update'])
        ){
            // check required fields
            if(
               empty($post['group_id']) ||
               empty($post['group_members']) ||
               empty($post['scheduled_date'])
            ){
                $link = add_query_arg(array(
                                        'message' => 'error_req'
                                    ), $_POST['_wp_http_referer']);
                wp_redirect($link);
                die;
            }

            // need to create a link to the post
            $id = $this->unban_create_update($post);
            if($id){ // success
                // redirect to edit page
                $link = add_query_arg(array(
                                        'message' => 'notice_saved',
                                        'action'  => 'edit',
                                        'id'      => $id
                                    ), $_POST['_wp_http_referer']);
            }else{ // error on saving
                // redirect to itself
                $link = add_query_arg(array(
                                        'message' => 'notice_error'
                                    ), $_POST['_wp_http_referer']);
            }
        }

        wp_redirect($link);
        die;
    }

    /** DASHBOARD PAGE *******************************************************************/

    /**
     * Register Dashboard page via the Settings API,
     * appends the setting to the tabs array of the object.
     */
    function register_dash_page() {
        $this->bpap_settings_tabs[$this->dash_settings_key] = __('Dashboard', 'bpap');

        register_setting( $this->dash_settings_key, $this->dash_settings_key );
        add_settings_section( 'dashboard_widgets', '', array(&$this, 'dash_out'), $this->dash_settings_key );

        add_action('bpap_dash_boxes', array(&$this, 'dash_general_stat'));
    }

    function dash_out(){ ?>
        <div id="dashboard-widgets-wrap">
            <div id="dashboard-widgets" class="metabox-holder columns-2">
                <?php do_action('bpap_dash_boxes') ?>
            </div><!-- /#dashboard-widgets -->
        </div><!-- /#dashboard-widgets-wrap -->
        <div class="clear"></div>
        <?php
    }

    function dash_general_stat(){
        $scheduled = $this->dash_general_stat_data(); ?>
        <div id="postbox-container-1" class="postbox-container">
            <div id="dashboard_right_now" class="postbox bpap">
                <h3><?php _e('Right Now', 'bpap'); ?></h3>
                <div class="inside">
                    <div class="table table_content">
                        <p class="sub"><?php _e('Scheduled', 'bpap'); ?></p>
                        <ul class="stat">
                            <?php if( bp_is_active('forums') && bp_forums_is_installed_correctly() ) { ?>
                                <li>
                                    <?php echo sprintf(
                                            _n('<span>%d</span> topic','<span>%d</span> topics', $scheduled['f_topics'], 'bpap') .
                                            ' ' .
                                            _n('in <span>%d</span> group', 'in <span>%d</span> groups', $scheduled['f_groups'], 'bpap'),
                                            $scheduled['f_topics'],
                                            $scheduled['f_groups']
                                        ); ?>
                                </li>
                            <?php } ?>

                            <li>
                                <?php echo sprintf(_n('<span>%d</span> site-wide notice','<span>%d</span> site-wide notices', $scheduled['notices'], 'bpap'), $scheduled['notices']); ?>
                            </li>

                            <li>
                                <?php echo sprintf(_n('unban members in <span>%d</span> group','unban members in <span>%d</span> groups', $scheduled['unban'], 'bpap'), $scheduled['unban']); ?>
                            </li>

                            <?php do_action('bpap_admin_dash_stat_li'); ?>
                        </ul>
                        <p class="description"><?php _e('Later other data will be here', 'bpap'); ?></p>
                    </div>
                    <br class="clear">
                </div><!-- /.inside -->
            </div><!-- /#dashboard_right_now -->
        </div><!-- /#postbox-container-1 -->
        <?php
    }

    /**
     * Get actuale counts for stat
     * @return array Array of data with numbers
     */
    function dash_general_stat_data(){
        /** @var $wpdb WPDB */
        global $wpdb;
        $scheduled   = array();
        $post_type_f = BPAP_FORUMS_TOPIC_TYPE;
        $post_type_n = BPAP_NOTICE_TYPE;
        $post_type_u = BPAP_UNBAN_TYPE;

        // get unique number of planned topics
        if( bp_is_active('forums') && bp_forums_is_installed_correctly() ) {
            $scheduled['f_topics'] = $wpdb->get_var($wpdb->prepare(
                    "SELECT COUNT(`ID`) FROM {$wpdb->posts}
                                    WHERE `post_type` = %s", $post_type_f
                )
            );

            // get unique number of groups that topics will be published in
            $scheduled['f_groups'] = $wpdb->get_var($wpdb->prepare(
                    "SELECT COUNT(DISTINCT `post_parent`) FROM {$wpdb->posts}
                                    WHERE `post_type` = %s", $post_type_f
                )
            );
        }
        // get unique number of groups that topics will be published in
        $scheduled['notices'] = $wpdb->get_var($wpdb->prepare(
                                    "SELECT COUNT(DISTINCT `ID`) FROM {$wpdb->posts}
                                    WHERE `post_type` = %s", $post_type_n));

        // get unique number of groups that members will be unbanned in
        $scheduled['unban'] = $wpdb->get_var($wpdb->prepare(
                                    "SELECT COUNT(DISTINCT `post_parent`) FROM {$wpdb->posts}
                                    WHERE `post_type` = %s", $post_type_u));

        return apply_filters('bpap_admin_dash_stat_data', $scheduled);
    }

    /** FORUMS TOPIC MANAGEMENT PAGES **************************************************/

    /**
     * Register Profiles page and appends the key to the theme settings tabs array.
     */
    function register_forums_page() {
        $this->bpap_settings_tabs[$this->forums_settings_key] = __('Forums Topics', 'bpap');

        register_setting( $this->forums_settings_key, $this->forums_settings_key );
        add_settings_section(
            'forums_data', // section id
            '', // title
            array(&$this, 'forums_out'), // method handler
            $this->forums_settings_key // slug
        );
    }

    /**
     * Give users ability to display the list of scheduled items
     * @return string Html of a page
     */
    function forums_out(){
        if(!isset($_GET['action']))
            $_GET['action'] = 'list';

        /** @var $item WP_POST */
        /** @var $wpdb WPDB */
        global $wpdb;

        switch ($_GET['action']) {
            // open a page to edit a topic
            case 'edit':
                // get the item data
                $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->posts} WHERE `ID` = %d", $_GET['id']));

                // display the html form with inserted data to edit
                include(BPAP_PATH . '/core/admin/forums-item.php');
                break;

            // open a page to add a topic
            case 'add':
                $item = bpap_get_empty_item(BPAP_FORUMS_TOPIC_TYPE);
                // display the html form
                include(BPAP_PATH . '/core/admin/forums-item.php');
                break;

            // delete an item
            case 'delete':
                if(is_numeric($_GET['id'])){
                    $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->posts} WHERE `ID` = %d", $_GET['id']));
                }
                $this->forums_list();
                break;

            // display the list
            case 'list':
            default:
                $this->forums_list();
                break;
        }
    }

    function forums_list(){
        // we need some extra forum admin class
        require_once(BPAP_PATH . '/core/admin/forums.php');

        echo '<h4>';
            _e('Scheduled Topics', 'bpap');
            echo '<a href="'.add_query_arg('action', 'add').'" class="add-new-h2">'.__('Add New', 'bpap').'</a>';
        echo '</h4>';

        //Prepare Table of elements
        $bpap_list_table = new BPAP_Forum_Topics_Table();
        $bpap_list_table->prepare_items();
        $bpap_list_table->display();
    }

    /**
     * Process data to create a new scheduled topic
     * @param array $post
     * @return integer $id ID of a topic that was created/updated (post type)
     */
    function forums_topic_create_update($post){
        if(!empty($post['tags']))
            $post['tags'] = str_replace(', ', ',', strip_tags(trim($post['tags'])));
        else
            $post['tags'] = '';

        $params = array(
                    'post_title'    => wp_strip_all_tags($post['title']),
                    'post_author'   => $post['author_id'],
                    'post_content'  => $post['content'],
                    'post_parent'   => $post['group_id'],
                    'post_date'     => $post['scheduled_date'],
                    'post_date_gmt' => get_gmt_from_date($post['scheduled_date']),
                    'post_excerpt'  => $post['tags'],
                    'post_status'   => 'bpap_pending',
                    'post_type'     => BPAP_FORUMS_TOPIC_TYPE,
                    // meta information
                    'post_content_filtered' => json_encode(array(
                                            'to_activity'   => isset($post['to_activity'])?$post['to_activity']:'no',
                                            'email_members' => isset($post['email_members'])?$post['email_members']:'no',
                                            'email_admins'  => isset($post['email_admins'])?$post['email_admins']:'no'
                                        ))
                );
        if(isset($post['ID']))
            $params['ID'] = $post['ID'];

        $id = wp_insert_post($params);

        if(is_numeric($id)){
            /** @var $wpdb WPDB */
            global $wpdb;
            $group = groups_get_group(array('group_id' => $post['group_id']));
            $topic = get_post($id);
            $link  = bp_get_group_forum_permalink($group).'/topic/'.$topic->post_name.'/';
            $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts} SET `guid` = %s WHERE `ID` = %d", $link, $id));
            return $id;
        }

        return false;
    }

    /** CALENDAR PAGE ******************************************************************/

    /**
     * Register the Calendar page with Calendario
     */
    function register_calendar_page(){
        $this->bpap_settings_tabs[$this->calendar_settings_key] = __('Calendar', 'bpap');

        register_setting( $this->calendar_settings_key, $this->calendar_settings_key );
        add_settings_section(
            'calendar', // section id
            '', // title
            array(&$this, 'calendar_out'), // method handler
            $this->calendar_settings_key // slug
        );
    }

    /**
     * Display the raw html
     */
    function calendar_out(){
        include(dirname(__FILE__).'/admin/html/calendar.html');
    }

    /** INFO/ABOUT PAGE ******************************************************************/

    /**
     * Register the Info page with some docs and FAQs
     */
    function register_info_page(){
        $this->bpap_settings_tabs[$this->info_settings_key] = __('About Author', 'bpap');

        register_setting( $this->info_settings_key, $this->info_settings_key );
        add_settings_section(
            'info', // section id
            '', // title
            array(&$this, 'info_out'), // method handler
            $this->info_settings_key // slug
        );
    }

    /**
     * Display the raw html
     */
    function info_out(){
        include(dirname(__FILE__).'/admin/html/about.html');
    }

    /** SETTINGS PAGE ********************************************************************/

    /**
     * Register the page
     */
    function register_settings_page(){
        $this->bpap_settings_tabs[$this->settings_settings_key] = __('Settings', 'bpap');

        register_setting( $this->settings_settings_key, $this->settings_settings_key );
        add_settings_section(
            'settings_notifications', // section id
            __('Email Notifications', 'bpap'), // title
            array(&$this, 'settings_notifications'), // method handler
            $this->settings_settings_key // slug
        );
        add_settings_section(
            'settings_emails', // section id
            __('Emails Subjects & Content', 'bpap'), // title
            array(&$this, 'settings_emails'), // method handler
            $this->settings_settings_key // slug
        );
        add_settings_section(
            'settings_deactivate', // section id
            __('Plugin Deactivation', 'bpap'), // title
            array(&$this, 'settings_deactivate'), // method handler
            $this->settings_settings_key // slug
        );
        add_settings_section(
            'settings_cron', // section id
            __('Cron Precision', 'bpap'), // title
            array(&$this, 'settings_cron'), // method handler
            $this->settings_settings_key // slug
        );

        do_action('bpap_admin_settings', $this);
    }

    /**
     * Display the notifications part
     */
    function settings_notifications(){
        // Description
        echo '<p>'.__('Please select in this section in which cases site admin want to receive email notifications.', 'bpap').'</p>';

        // Checkboxes with selection ?>
        <label for="notify_email_topics" class="full_label">
            <?php
            $checked_topics = (isset($this->bpap['notify']['email']) && in_array('f_topics', $this->bpap['notify']['email']))?'checked="checked"':'';
            ?>
            <input type="checkbox" <?php echo $checked_topics; ?> name="bpap[notify][email][]" id="notify_email_topics" value="f_topics" />&nbsp;
            <?php _e('Published groups forum topics', 'bpap'); ?>
        </label>

        <label for="notify_email_notices" class="full_label">
            <?php
            $checked_notices = (isset($this->bpap['notify']['email']) && in_array('notices', $this->bpap['notify']['email']))?'checked="checked"':'';
            ?>
            <input type="checkbox" <?php echo $checked_notices; ?> name="bpap[notify][email][]" id="notify_email_notices" value="notices" />&nbsp;
            <?php _e('Published site-wide notices', 'bpap'); ?>
        </label>

        <label for="notify_email_unban" class="full_label">
            <?php
            $checked_unban = (isset($this->bpap['notify']['email']) && in_array('unban', $this->bpap['notify']['email']))?'checked="checked"':'';
            ?>
            <input type="checkbox" <?php echo $checked_unban; ?> name="bpap[notify][email][]" id="notify_email_unban" value="unban" />&nbsp;
            <?php _e('Members were unbaned', 'bpap'); ?>
        </label>

        <?php echo '<p class="description p_margin">'.__('Please take into account: the more you select - the more emails you will get.', 'bpap').'</p>'; ?>

        <div class="clear"></div>

        <input type="submit" class="button" name="bpap[action][settings][notify]" value="<?php _e('Save notifications', 'bpap'); ?>" />
        <?php
    }

    /**
     * Display the emails content part
     */
    function settings_emails(){
        // Description
        echo '<p>'.__('You can specify here emails subjects and content that will be sent to users in some cases.', 'bpap').'</p>'; ?>

        <table style="width: 90%;">
            <tr><td colspan="2"><h5><?php _e('Forum Topic Published', 'bpap'); ?></h5></td></tr>
            <tr>
                <td style="width:15%"><?php _e('Subject:', 'bpap') ?></td>
                <td>
                    <input style="width:250px" type="text" name="bpap[emails][f_topics][subject]" value="<?php esc_attr_e($this->bpap['emails']['f_topics']['subject']); ?>" />
                </td>
            </tr>
            <tr>
                <td><?php _e('Content:', 'bpap') ?></td>
                <td>
                    <textarea name="bpap[emails][f_topics][content]" style="width:60%;height:100px"><?php echo stripslashes(str_replace(array('<br />','<br/>','<br>'), "\r\n", $this->bpap['emails']['f_topics']['content'])); ?></textarea>
                </td>
            </tr>
        </table>
        <p class="description"><?php _e('All instances of <code>%TOPIC_LINK%</code>, <code>%GROUP_LINK%</code>, <code>%TOPIC_TITLE%</code>, <code>%GROUP_TITLE%</code> will be replaced with appropriate text.', 'bpap');?></p>

        <div class="clear"></div>

        <input type="submit" class="button" name="bpap[action][settings][emails]" value="<?php _e('Save emails', 'bpap'); ?>" />
        <?php
    }

    /**
     * Display the deactivation options
     */
    function settings_deactivate(){
        // Description
        echo '<p>'.__('Specify what to do with all the data when the plugin will be deactivated.', 'bpap').'</p>';

        // Default value
        if(!isset($this->bpap['deactivate'])) $this->bpap['deactivate'] = 'save';

        // Radio-buttons with selection ?>
        <label for="bpap_deactivate_save" class="full_label">
            <?php $checked_save = ($this->bpap['deactivate'] == 'save')?'checked="checked"':''; ?>
            <input type="radio" <?php echo $checked_save; ?> name="bpap[deactivate]" id="bpap_deactivate_save" value="save" />&nbsp;
            <?php _e('Do not delete data - just deactivate the plugin', 'bpap'); ?>
        </label>

        <label for="bpap_deactivate_delete" class="full_label">
            <?php $checked_delete = ($this->bpap['deactivate'] == 'delete')?'checked="checked"':''; ?>
            <input type="radio" <?php echo $checked_delete; ?> name="bpap[deactivate]" id="bpap_deactivate_delete" value="delete" />&nbsp;
            <?php _e('Delete all plugin data (settings, all scheduled content etc)', 'bpap'); ?>
        </label>

        <?php echo '<p class="description p_margin">'.__('All scheduled content will NOT be published if you select to delete it.', 'bpap').'</p>'; ?>

        <div class="clear"></div>

        <input type="submit" class="button" name="bpap[action][settings][deactivate]" value="<?php _e('Save deactivation options', 'bpap'); ?>" />
        <?php
    }

    /**
     * Change the cron precision
     */
    function settings_cron(){
        if(!isset($this->bpap['cron']) || empty($this->bpap['cron']))
            $this->bpap['cron'] = '300';

        // Description
        echo '<p>'.__('You can specify in this section how precise should be cron.', 'bpap').'</p>';
        // Checkboxes with selection ?>
        <label for="cron_precision_2min" class="full_label">
            <?php $checked = ('120' == $this->bpap['cron'])?'checked="checked"':''; ?>
            <input type="radio" <?php echo $checked; ?> name="bpap[cron]" id="cron_precision_2min" value="120" />&nbsp;
            <?php _e('Every 2 minutes', 'bpap'); ?>
        </label>
        <label for="cron_precision_5min" class="full_label">
            <?php $checked = ('300' == $this->bpap['cron'])?'checked="checked"':''; ?>
            <input type="radio" <?php echo $checked; ?> name="bpap[cron]" id="cron_precision_5min" value="300" />&nbsp;
            <?php _e('Every 5 minutes', 'bpap'); ?>
        </label>
        <label for="cron_precision_10min" class="full_label">
            <?php $checked = ('600' == $this->bpap['cron'])?'checked="checked"':''; ?>
            <input type="radio" <?php echo $checked; ?> name="bpap[cron]" id="cron_precision_10min" value="600" />&nbsp;
            <?php _e('Every 10 minutes', 'bpap'); ?>
        </label>
        <label for="cron_precision_30min" class="full_label">
            <?php $checked = ('1800' == $this->bpap['cron'])?'checked="checked"':''; ?>
            <input type="radio" <?php echo $checked; ?> name="bpap[cron]" id="cron_precision_30min" value="1800" />&nbsp;
            <?php _e('Every 30 minutes', 'bpap'); ?>
        </label>
        <label for="cron_precision_60min" class="full_label">
            <?php $checked = ('3600' == $this->bpap['cron'])?'checked="checked"':''; ?>
            <input type="radio" <?php echo $checked; ?> name="bpap[cron]" id="cron_precision_60min" value="3600" />&nbsp;
            <?php _e('Every 60 minutes', 'bpap'); ?>
        </label>

        <?php echo '<p class="description p_margin">'.__('Please take into account: the less number of minutes you select - the more load and cpu it will take.', 'bpap').'</p>'; ?>

        <div class="clear"></div>

        <!-- <input type="hidden" name="bpap[action][settings]" value="cron" /> -->
        <input type="submit" class="button" name="bpap[action][settings][cron]" value="<?php _e('Save cron precision', 'bpap'); ?>" />
        <?php
    }

    /** NOTIFICATIONS MANAGER ************************************************************/

    /**
     * Register the page
     */
    function register_notify_page(){
        $this->bpap_settings_tabs[$this->notices_settings_key] = __('Notices', 'bpap');

        register_setting( $this->notices_settings_key, $this->notices_settings_key );
        add_settings_section(
            'notices_list', // section id
            '', // title
            array(&$this, 'notices_out'), // method handler
            $this->notices_settings_key // slug
        );
    }

    /**
     * Display the page
     */
    function notices_out(){
        if(!isset($_GET['action']))
            $_GET['action'] = 'list';

        switch ($_GET['action']) {
            // open a page to edit an item
            case 'edit':
                // get the item data
                global $wpdb;
                $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->posts} WHERE `ID` = %d", $_GET['id']));

                // display the html form with inserted data to edit
                include(BPAP_PATH . '/core/admin/notice-item.php');
                break;

            // open a page to add an item
            case 'add':
                $item = bpap_get_empty_item(BPAP_NOTICE_TYPE);
                // display the html form
                include(BPAP_PATH . '/core/admin/notice-item.php');
                break;

            // delete an item
            case 'delete':
                global $wpdb;
                if(is_numeric($_GET['id'])){
                    $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->posts} WHERE `ID` = %d", $_GET['id']));
                }
                $this->notices_list();
                break;

            // display the list
            case 'list':
            default:
                $this->notices_list();
                break;
        }
    }

    function notices_list(){
        // we need some extra forum admin class
        require_once(BPAP_PATH . '/core/admin/notices.php');

        echo '<h4>';
            _e('Scheduled Notices', 'bpap');
            echo '<a href="'.add_query_arg('action', 'add').'" class="add-new-h2">'.__('Add New', 'bpap').'</a>';
        echo '</h4>';

        //Prepare Table of elements
        $bpap_list_table = new BPAP_Notices_Table();
        $bpap_list_table->prepare_items();
        $bpap_list_table->display();
    }

    /**
     * Process the save
     */
    /**
     * Process data to create a new scheduled topic
     * @return integer $id ID of a topic that was created/updated (post type)
     */
    function notices_create_update($post){
        $params = array(
                    'post_title'    => wp_strip_all_tags($post['subject']),
                    'post_author'   => $post['author_id'],
                    'post_content'  => $post['message'],
                    'post_date'     => $post['scheduled_date'],
                    'post_date_gmt' => get_gmt_from_date($post['scheduled_date']),
                    'post_status'   => 'bpap_pending',
                    'post_type'     => BPAP_NOTICE_TYPE
                );
        if(isset($post['ID']))
            $params['ID'] = $post['ID'];

        $id = wp_insert_post($params);

        if(is_numeric($id))
            return $id;

        return false;
    }

    /** UNBAN GROUPS MEMBERS *************************************************************/

    /**
     * Register a page
     */
    function register_unban_page(){
        $this->bpap_settings_tabs[$this->unban_settings_key] = __('Unban Members', 'bpap');

        register_setting( $this->unban_settings_key, $this->unban_settings_key );
        add_settings_section(
            'unban_list', // section id
            '', // title
            array(&$this, 'unban_out'), // method handler
            $this->unban_settings_key // slug
        );
    }

    /**
     * Display the page
     */
    function unban_out(){
        if(!isset($_GET['action']))
            $_GET['action'] = 'list';

        switch ($_GET['action']) {
            // open a page to edit an item
            case 'edit':
                // get the item data
                /** @var $wpdb WPDB */
                global $wpdb;
                /** @var $item WP_POST */
                $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM {$wpdb->posts} WHERE `ID` = %d", (int) $_GET['id']));

                // display the html form with inserted data to edit
                include(BPAP_PATH . '/core/admin/unban-item.php');
                break;

            // open a page to add an item
            case 'add':
                /** @var $item WP_POST */
                $item = bpap_get_empty_item(BPAP_NOTICE_TYPE);
                // display the html form
                include(BPAP_PATH . '/core/admin/unban-item.php');
                break;

            case 'delete':
                global $wpdb;
                if(is_numeric($_GET['id'])){
                    $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->posts} WHERE `ID` = %d", (int) $_GET['id']));
                }
                $this->unban_list();
                break;

            // display the list
            case 'list':
            default:
                $this->unban_list();
                break;
        }
    }

    function unban_list(){
        // we need some extra forum admin class
        require_once(BPAP_PATH . '/core/admin/unban.php');

        echo '<h4>';
            _e('Scheduled Unban Actions', 'bpap');
            echo '<a href="'.add_query_arg('action', 'add').'" class="add-new-h2">'.__('Add New', 'bpap').'</a>';
        echo '</h4>';

        //Prepare Table of elements
        $bpap_list_table = new BPAP_Unban_Table();
        $bpap_list_table->prepare_items();
        $bpap_list_table->display();
    }

    /**
     * Save / Update unban actions
     * @param $post
     * @return bool|int|\WP_Error
     */
    function unban_create_update($post){
        // prepare the data before save
        switch($post['group_members']){
            case 'some':
                $post['content'] = json_encode($post['unban_user_ids']);
                break;

            case 'all':
            default:
                $post['content'] = json_encode(array());
                break;
        }

        $params = array(
                    'post_parent'   => $post['group_id'],
                    'post_content'  => $post['content'],
                    'post_excerpt'  => $post['group_members'],
                    'post_date'     => $post['scheduled_date'],
                    'post_date_gmt' => get_gmt_from_date($post['scheduled_date']),
                    'post_status'   => 'bpap_pending',
                    'post_type'     => BPAP_UNBAN_TYPE
                );
        if ( isset($post['ID']) ) {
            $params['ID'] = $post['ID'];
        }

        $id = wp_insert_post($params);

        if ( is_numeric($id) ) {
            return $id;
        }

        return false;
    }

    /** GENERAL ADMIN CODE ***************************************************************/

    /**
     * Adds a page with all subpages (tabs)
     */
    function add_admin_menu() {
        $page = add_submenu_page(
                is_multisite()?'settings.php':'options-general.php',
                __('BP Auto Posting', 'bpap'),
                __('BP Auto Posting', 'bpap'),
                'edit_users',
                $this->bpap_options_key,
                array(&$this, 'bpap_page' )
            );

        bpap_load_admin_assets($page);
    }

    /**
     * BPAP page rendering goes here, checks for active tab.
     */
    function bpap_page() {
        $tab = isset( $_GET['tab'] ) ? $_GET['tab'] : $this->dash_settings_key; ?>
        <div id="bpap-admin" class="wrap">
            <?php $this->bpap_header(); ?>
            <form method="post" action="" enctype="multipart/form-data" class="formee">
                <?php
                wp_nonce_field( 'bpap-update-options' );
                //settings_fields( $tab );
                do_settings_sections( $tab );
                //submit_button();
                ?>
            </form>
            <div class="clearfix"></div>
            <?php $this->bpap_footer(); ?>
        </div>
        <?php
    }

    /**
     * Renders header titles and tabs on the BPPS page.
     */
    function bpap_header() {
        $current_tab = isset($_GET['tab']) ? $_GET['tab'] : $this->dash_settings_key;

        echo '<div id="icon-bpap" class="icon32"><br></div>';

        echo '<h2>';
            echo '<span class="divider">';
                _e('BP Auto Posting','bpap'); echo ' <em><sup><a href="http://ovirium.com/portfolio/bp-auto-posting/" target="_blank">v'. BPAP_VER .'</a></sup></em>';
            echo '</span>';
            echo '<em>'. __('Schedule Content','bpap') .'</em>';
        echo '</h2>';

        $this->notices();

        echo '<h3 class="nav-tab-wrapper">';
            foreach ( $this->bpap_settings_tabs as $tab_key => $tab_caption ) {
                $active = $current_tab == $tab_key ? 'nav-tab-active' : '';
                $calendar = $tab_key == 'calendar' ? 'calendar' : '';
                echo '<a class="nav-tab ' . $calendar . ' ' . $active . '" href="?page=' . $this->bpap_options_key . '&tab=' . $tab_key . '">' . $tab_caption . '</a>';
            }
        echo '</h2>';
    }

    /**
     * Footer on each and every page
     */
    function bpap_footer(){ ?>
        <div id="bottom">
            <a href="http://ovirium.com/support/bp-auto-posting/" target="_blank"><?php _e('Support & Features Request', 'bpap'); ?></a>
            | <a href="http://ovirium.com/">Ovirium Production</a>
            | <a href="http://twitter.com/slaFFik" target="_blank" title="Twitter">@slaFFik</a>
        </div>
        <?php
    }

    /** NOTICES *************************************************************/

    /**
     * Notices that will be fired on some events
     */
    function notices(){
        $message = false;

        if(isset($_COOKIE['bpap_message'])){
            $message = $_COOKIE['bpap_message'];
        }

        if(isset($_GET['message']))
            $message = $_GET['message'];

        if(!$message)
            return;

        switch($message){
            case 'topic_error':
                echo '<div class="error">
                   <p>'.__('There was an error while saving topic data, please try again.', 'bpap').'</p>
                </div>';
                break;
            case 'topic_saved':
                echo '<div class="updated">
                   <p>'.__('The topic was successfully saved.', 'bpap').'</p>
                </div>';
                break;
            case 'topics_deleted':
                echo '<div class="updated">
                   <p>'.__('Selected topics were successfully deleted.', 'bpap').'</p>
                </div>';
                break;

            case 'notice_error':
                echo '<div class="error">
                   <p>'.__('There was an error while saving notice data, please try again.', 'bpap').'</p>
                </div>';
                break;
            case 'notice_saved':
                echo '<div class="updated">
                   <p>'.__('Saved successfully', 'bpap').'</p>
                </div>';
                break;
            case 'notices_deleted':
                echo '<div class="updated">
                   <p>'.__('Selected notices were successfully deleted.', 'bpap').'</p>
                </div>';
                break;

            case 'error_req':
                echo '<div class="error">
                   <p>'.__('Required fields are empty. Please fill them in.', 'bpap').'</p>
                </div>';
                break;

            case 'item_deleted':
                echo '<div class="updated">
                   <p>'.__('The item was successfully deleted.', 'bpap').'</p>
                </div>';
                break;

            case 'settings_saved':
                echo '<div class="updated">
                   <p>'.__('Settings were successfully saved.', 'bpap').'</p>
                </div>';
                break;
            case 'settings_saved_error':
                echo '<div class="error">
                   <p>'.__('Nothing to save OR there was an error while saving settings.', 'bpap').'</p>
                </div>';
                break;
        }
    }

};

// Initialize BPAP Options
new BPAP_Admin_Tabs;

?>