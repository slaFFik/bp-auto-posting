<?php
/**
 * Adding new сron interval for publishing
 */
add_filter('cron_schedules', 'bpap_cron_add_schedules');
function bpap_cron_add_schedules($schedules) {
    $bpap = get_option('bpap');

    if(!isset($bpap['cron']) || empty($bpap['cron']))
        $bpap['cron'] = '300';

    $schedules[$bpap['cron'].'sec'] = array(
        'interval' => $bpap['cron'],
        'display'  => sprintf(__('Every %d minutes', 'bpap'), $bpap['cron'] / 60)
    );

    return $schedules;
}

/**
  * Create all cron jobs
  */
function bpap_cron_create($cron_time = false){
    $cron_topics  = wp_next_scheduled('bpap_cron_forum_topics');
    $cron_notices = wp_next_scheduled('bpap_cron_notices');
    $cron_unban   = wp_next_scheduled('bpap_cron_unban');

    // both exists
    if($cron_topics && $cron_notices && $cron_unban)
        return;

    $bpap = get_option('bpap');

    if(!$cron_time || !is_numeric($cron_time)){
        if(!isset($bpap['cron']) || empty($bpap['cron']))
            $bpap['cron'] = '300';
    }else{
        $bpap['cron'] = $cron_time;
    }

    if(!$cron_topics)
        wp_schedule_event(time() + 60, $bpap['cron'].'sec', 'bpap_cron_forum_topics');

    if(!$cron_notices)
        wp_schedule_event(time() + 120, $bpap['cron'].'sec', 'bpap_cron_notices');

    if(!$cron_unban)
        wp_schedule_event(time() + 180, $bpap['cron'].'sec', 'bpap_cron_unban');
}

/**
 * Delete all cron jobs
 */
function bpap_cron_clear(){
    // remove all cron jobs
    $timestamp = wp_next_scheduled('bpap_cron_forum_topics');
    wp_unschedule_event($timestamp, 'bpap_cron_forum_topics');

    $timestamp = wp_next_scheduled('bpap_cron_notices');
    wp_unschedule_event($timestamp, 'bpap_cron_notices');

    $timestamp = wp_next_scheduled('bpap_cron_unban');
    wp_unschedule_event($timestamp, 'bpap_cron_unban');

    wp_clear_scheduled_hook('bpap_cron_forum_topics');
    wp_clear_scheduled_hook('bpap_cron_notices');
    wp_clear_scheduled_hook('bpap_cron_unban');
}

/**
 * Process Forum topics cron
 */
function bpap_cron_forum_topics(){
    /** @var $wpdb WPDB */
    global $wpdb;

    // Check that forums for groups are activated and work properly
    if( !bp_is_active('forums') || !bp_forums_is_installed_correctly() )
       return false;

    $successful   = $planned = $ids = array();
    $success_list = '';

    // get all future post types with datetime in the future
    $post_type = BPAP_FORUMS_TOPIC_TYPE;
    $cur_date  = current_time('mysql', 1); // get the GMT

    $planned = $wpdb->get_results($wpdb->prepare(
                        "SELECT * FROM `{$wpdb->posts}`
                        WHERE `post_type` = %s
                          AND `post_date_gmt` < (%s + INTERVAL 5 MINUTE + INTERVAL 1 SECOND);",
                        $post_type,
                        $cur_date));

    if ( empty($planned) )
        return false;

    // post the scheduled items
    foreach($planned as $post){
        $topic           = new Stdclass;
        $display_name    = bp_core_get_user_displayname($post->post_author);
        $forum_id        = groups_get_groupmeta($post->post_parent, 'forum_id');
        $meta            = json_decode($post->post_content_filtered);
        $topic->topic_slug    = $post->post_name;
        $topic->topic_title   = $post->post_title;
        $topic->topic_content = $post->post_content;
        $topic->group_id      = $post->post_parent;
        $topic->topic_tags    = $post->post_excerpt;
        $topic->to_activity   = $meta->to_activity;
        $topic->email_members = $meta->email_members;
        $topic->email_admins  = $meta->email_admins;
        $topic->topic_id      = bp_forums_new_topic(array(
                                    'topic_title'            => $post->post_title,
                                    'topic_slug'             => $post->post_name,
                                    'topic_text'             => $post->post_content,
                                    'topic_poster'           => $post->post_author,
                                    'topic_poster_name'      => $display_name,
                                    'topic_last_poster'      => $post->post_author,
                                    'topic_last_poster_name' => $display_name,
                                    // 'topic_start_time'       => get_gmt_from_date($topic->post_date),
                                    // 'topic_time'             => get_gmt_from_date($topic->post_date),
                                    'topic_tags'             => $post->post_excerpt, // array or comma
                                    'forum_id'               => $forum_id
                                ));
        $ids[$post->ID] = $topic;
    }

    if ( empty($ids) )
        return false;

    // get the settings
    $bpap = get_option('bpap');

    // now delete all successfully posted topics drafts
    foreach($ids as $post_id => $topic){
        if(is_object($topic)){
            // force remove -  bypass the bin
            wp_delete_post($post_id, true);

            // group data
            $group       = groups_get_group(array('group_id' => $topic->group_id));
            $group->link = bp_get_group_permalink($group);

            $topic->topic_link = bp_get_group_forum_permalink($group).'/topic/'.$topic->topic_slug.'/';

            // format the html for notification email
            $success_list .= ' - <a href="' . $topic->topic_link . '">' . $topic->topic_title . '</a> [<a href="' . $group->link . '">' . esc_attr($group->name) . '</a>]'."\r\n";

            // post to activity feed if that was enabled
            if($topic->to_activity == 'yes'){
                $activity_action = sprintf(__( '%1$s started the forum topic %2$s in the group %3$s', 'bpap'),
                                        bp_core_get_userlink( $post->post_author ),
                                        '<a href="' . $topic->topic_link .'">' . esc_attr( $topic->topic_title ) . '</a>',
                                        '<a href="' . $group->link . '">' . esc_attr( $group->name ) . '</a>' );
                $activity_content = bp_create_excerpt( $topic->topic_content );

                // Record this in activity stream
                groups_record_activity( array(
                    'action'            => apply_filters_ref_array( 'groups_activity_new_forum_topic_action',  array( $activity_action,  $topic->topic_content, &$topic ) ),
                    'content'           => apply_filters_ref_array( 'groups_activity_new_forum_topic_content', array( $activity_content, $topic->topic_content, &$topic ) ),
                    'primary_link'      => apply_filters( 'groups_activity_new_forum_topic_primary_link', $group->link . 'forum/topic/' . $topic->topic_slug . '/' ),
                    'type'              => 'new_forum_topic',
                    'item_id'           => $group->id,
                    'secondary_item_id' => $topic->topic_id
                ) );
            }

            // send email to group members if that was enabled
            if($topic->email_members == 'yes'){
                // get all members: group_id, limit, page, exclude_admins_mods
                $all = BP_Groups_Member::get_all_for_group( $topic->group_id, false, false, false );

                foreach($all['members'] as $member){
                    $content = str_replace(
                                    array('%TOPIC_LINK%', '%GROUP_LINK%', '%TOPIC_TITLE%', '%GROUP_TITLE%'),
                                    array(
                                        $topic->topic_link,
                                        $group->link,
                                        $topic->topic_title,
                                        $group->name
                                    ),
                                    htmlspecialchars_decode($bpap['emails']['f_topics']['content'])
                                );
                    wp_mail($member->user_email, $bpap['emails']['f_topics']['subject'], nl2br(stripslashes($content)), 'Content-type: text/html;' . "\r\n");
                }
            }

            // send email to group admins only if that was enabled
            if($topic->email_admins == 'yes'){
                $admins_ids = $emails = array();
                // get admins
                $admin_ids_raw = groups_get_group_admins($topic->group_id);
                // ignore other data - we need just ids
                foreach($admin_ids_raw as $admin)
                    $admins_ids[] = $admin->user_id;
                // format the string 4 sql
                if (!empty($admins_ids) ) {
                    $admins_ids_str = implode(',', $admins_ids);
                    $emails = $wpdb->get_col("SELECT `user_email` FROM {$wpdb->users} WHERE `ID` IN ({$admins_ids_str})");
                }

                // send emails
                foreach($emails as $email){
                    $content = str_replace(
                                    array('%TOPIC_LINK%', '%GROUP_LINK%', '%TOPIC_TITLE%', '%GROUP_TITLE%'),
                                    array(
                                        $topic->topic_link,
                                        $group->link,
                                        $topic->topic_title,
                                        $group->name
                                    ),
                                    htmlspecialchars_decode($bpap['emails']['f_topics']['content'])
                                );
                    wp_mail($email, $bpap['emails']['f_topics']['subject'], nl2br(stripslashes($content)), 'Content-type: text/html;' . "\r\n");
                }
            }
        }
    }

    // send email to admin only if we enabled this and we have what to send
    if(isset($bpap['notify']['email']) && in_array('f_topics', $bpap['notify']['email']) && !empty($success_list)){
        // send notification about what forum topics were published
        $email   = get_option('admin_email');
        $subject = __('Groups Forums Topics Published','bpap');
        $content = sprintf(__("Below is the list of topics, that have just been posted on the '%s' site:

%s

-----------------

This message was automatically sent to you by a 'BuddyPress Auto Posting' plugin.", 'bpap'),
                        get_bloginfo('name'),
                        $success_list
                    );
        wp_mail($email, $subject, nl2br($content), 'Content-type: text/html;' . "\r\n");
    }
}
add_action('bpap_cron_forum_topics', 'bpap_cron_forum_topics');

/**
 * Process Notices cron
 */
function bpap_cron_notices(){
    /** @var $wpdb WPDB */
    global $wpdb;

    // Check that required component is activated
    if(!bp_is_active('messages'))
       return false;

    $successful   = $planned = $ids = array();
    $success_list = '';

    // get all future post types with datetime in the future
    $post_type = BPAP_NOTICE_TYPE;
    $cur_date  = current_time('mysql', 1); // get the GMT

    $planned = $wpdb->get_results($wpdb->prepare(
                        "SELECT * FROM `{$wpdb->posts}`
                        WHERE `post_type` = %s
                          AND `post_date_gmt` < (%s + INTERVAL 5 MINUTE + INTERVAL 1 SECOND);",
                        $post_type,
                        $cur_date));

    if(empty($planned))
        return false;

    // post the scheduled items
    foreach($planned as $post){
        $notice            = new BP_Messages_Notice;
        $notice->subject   = $post->post_title;
        $notice->message   = $post->post_content;
        $notice->date_sent = $post->post_date_gmt;
        $notice->is_active = 1;
        if($notice->save()){ // was successfully sent
            $ids[] = $notice;
            // force delete the source post - bypass the bin
            wp_delete_post($post->ID, true);
        }

        do_action_ref_array( 'messages_send_notice', array( $post->post_title, $post->post_content ) );
    }

    if(empty($ids))
        return false;

    // now prepate email body
    foreach($ids as $notice){
        // format the html for notification email
        $success_list .= ' - ' . $notice->subject . "\r\n";
    }

    // get the settings
    $bpap = get_option('bpap');

    // send email only if we enabled this and we have what to send
    if(isset($bpap['notify']['email']) && in_array('notices', $bpap['notify']['email']) && !empty($success_list)){
        // send notification about what forum topics were published
        $email   = get_option('admin_email');
        $subject = __('Site-wide Notices Published','bpap');
        $content = sprintf(__("Below are notices, that have just been posted on the '%s' site:

%s

-----------------

This message was automatically sent to you by a 'BuddyPress Auto Posting' plugin.", 'bpap'),
                        get_bloginfo('name'),
                        $success_list
                    );
        wp_mail($email, $subject, nl2br($content), 'Content-type: text/html;' . "\r\n");
    }
}
add_action('bpap_cron_notices', 'bpap_cron_notices');

/**
 * Process Unban cron
 */
function bpap_cron_unban(){
    /** @var $wpdb WPDB */
    global $wpdb;

    // Check that required component is activated
    if(!bp_is_active('groups'))
       return false;

    $successful   = $planned = $ids = array();
    $success_list = '';

    // get all future post types with datetime in the future
    $post_type = BPAP_UNBAN_TYPE;
    $cur_date  = current_time('mysql', 1); // get the GMT

    $planned = $wpdb->get_results($wpdb->prepare(
                        "SELECT * FROM `{$wpdb->posts}`
                        WHERE `post_type` = %s
                          AND `post_date_gmt` < (%s + INTERVAL 5 MINUTE + INTERVAL 1 SECOND);",
                        $post_type,
                        $cur_date));

    if(empty($planned))
        return false;

    // post the scheduled items
    foreach($planned as $post){
        $unban            = new Stdclass;
        $unbanned         = array();
        $unban->type      = $post->post_excerpt;
        $unban->members   = $post->post_content;
        $unban->group_id  = $post->post_parent;

        $member_ids = json_decode($unban->members);
        // loop through banned members to unban all of them
        foreach($member_ids as $member_id){
            $unbanning = new BP_Groups_Member($member_id, $unban->group_id);
            $unbanning->is_banned = 0;
            groups_update_groupmeta(
                    $unban->group_id,
                    'total_member_count',
                    ( (int) groups_get_groupmeta( $unban->group_id, 'total_member_count' ) + 1 ) );
            bp_update_user_meta(
                    $member_id,
                    'total_group_count',
                    (int) bp_get_user_meta( $member_id, 'total_group_count', true ) + 1 );
            if($unbanning->save()){ // was unbanned
                $unbanned[] = $member_id;
            }
            unset($unbanning);
        }
        // check that everything went well
        if(count($member_ids) == count($unbanned)){
            $ids[] = $unban;
            wp_delete_post($post->ID, true);
        }
    }

    if(empty($ids))
        return false;

    // now prepate email body
    foreach($ids as $unban){
        // format the html for notification email
        $list = __('N/a', 'bpap');
        if($unban->type == 'all')
            $list = __('All users will be unbanned', 'bpap');
        else{
            $users_ids = json_decode($unban->members);
            $users_fullname = array();
            foreach($users_ids as $user_id){
                $users_fullname[] = bp_core_get_userlink($user_id);
            }
            $list = implode(', ', $users_fullname);
        }

        $group       = new Stdclass;
        $group_link  = '';
        if(is_numeric($unban->group_id) && ($unban->group_id > 0)){
            $group      = groups_get_group(array('group_id' => $unban->group_id));
            $group_link = '<a href="'.bp_get_group_permalink($group).'" target="_blank">'.$group->name.'</a>';
        }

        $success_list .= ' - ' . $list . ' ' . __('in', 'bpap') . ' ' . $group_link . "\r\n";
    }

    // get the settings
    $bpap = get_option('bpap');

    // send email only if we enabled this and we have what to send
    if(isset($bpap['notify']['email']) && in_array('unban', $bpap['notify']['email']) && !empty($success_list)){
        // send notification about members unbanning
        $email   = get_option('admin_email');
        $subject = __('Group Members Unbanned','bpap');
        $content = sprintf(__("Below is the list of users that were unbanned on the '%s' site:

%s

-----------------

This message was automatically sent to you by a 'BuddyPress Auto Posting' plugin.", 'bpap'),
                        get_bloginfo('name'),
                        $success_list
                    );
        wp_mail($email, $subject, nl2br($content), 'Content-type: text/html;' . "\r\n");
    }
}
add_action('bpap_cron_unban', 'bpap_cron_unban');
