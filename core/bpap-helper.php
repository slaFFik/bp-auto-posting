<?php

/**
 * Create the empty forum topic item object
 * @param bool $post_type
 * @return \Stdclass
 */
function bpap_get_empty_item($post_type = false){
    $item = new Stdclass;

    $item->ID                    = '';
    $item->post_title            = '';
    $item->post_author           = '';
    $item->post_content          = '';
    $item->post_name             = '';
    $item->post_date             = '';
    $item->post_excerpt          = '';
    $item->guid                  = '';
    $item->post_content_filtered = '';
    $item->post_parent           = '';
    $item->post_status           = 'bpap_pending';
    $item->post_type             = $post_type;

    return $item;
}

/**
 * Get action Edit / Delete for lists of items
 * @param $item
 * @return string
 */
function bpap_get_row_item_action($item){
    return '<div class="row-actions">
                <span class="edit">
                    <a href="'.add_query_arg(array('action' => 'edit', 'id' => $item->ID)).'" title="'.__('Click to edit', 'bpap').'">'.__('Edit', 'bpap').'</a> |
                </span>
                <span class="delete">
                    <a href="'.add_query_arg(array('action' => 'delete', 'id' => $item->ID)).'" title="'.__('Click to delete', 'bpap').'" class="delete">'.__('Delete', 'bpap').'</a>
                </span>
            </div>';
}

/**
 * Get groups where we have banned members
 */
function bpap_get_groups_with_banned(){
    /** @var $wpdb WPDB */
    global $wpdb, $bp;

    // select groups
    $ids = $wpdb->get_results("SELECT gm.`group_id`, gd.`name`
                                FROM {$bp->groups->table_name_members} AS gm
                                LEFT JOIN {$bp->groups->table_name} as gd ON gd.`id` = gm.`group_id`
                                WHERE gm.`is_banned` = 1
                                GROUP BY gm.`group_id`
                                ORDER BY gd.`name` ASC");

    if ( !empty($ids) ) {
        return $ids;
    }

    return array();
}

/**
 * Get banned members in a group
 * @param $group_id
 * @return array
 */
function bpap_get_banned_members_in_group($group_id){
    /** @var $wpdb WPDB */
    global $wpdb, $bp;

    $ids = array();

    if ( !is_numeric($group_id) ){
        return $ids;
    }

    // select members
    $ids = $wpdb->get_results($wpdb->prepare(
                                    "SELECT gm.`user_id`, u.`display_name`
                                    FROM {$bp->groups->table_name_members} AS gm
                                    LEFT JOIN {$wpdb->users} as u ON u.`ID` = gm.`user_id`
                                    WHERE gm.`is_banned` = 1
                                        AND gm.`group_id` = %d
                                    ORDER BY u.`display_name` ASC",
                                    $group_id));

    if ( !empty($ids) ) {
        return $ids;
    }

    return $ids;
}