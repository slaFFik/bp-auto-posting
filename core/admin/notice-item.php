<fieldset>
    <legend><?php 
        if(isset($_GET['id']) && is_numeric($_GET['id'])){
            _e('Update The Notice', 'bpap');
        }else{
            _e('Create New Notice', 'bpap');
        }
    ?></legend>

    <div class="grid-12-12">
        <label class="major"><?php _e('Notice Subject', 'bpap'); ?> <em class="formee-req">*</em></label>
        <input type="text" id="notice_subject" name="bpap[subject]" value="<?php esc_attr_e($item->post_title); ?>">
    </div>

    <script type="text/javascript">jQuery('#notice_subject').focus();</script>
    
    <div class="grid-12-12">
        <label class="major"><?php _e('Message', 'bpap'); ?> <em class="formee-req">*</em></label>
        <?php wp_editor(
                $item->post_content, // content text
                'message', // id="content"
                array(
                    'media_buttons' => false,
                    'textarea_name' => 'bpap[message]'
                )); ?>
    </div>
    
    <div class="grid-12-12">
        <label class="major"><?php _e('When the notice should be published?', 'bpap'); ?> <em class="formee-req">*</em></label>
        <input type="text" class="formee-small datetimepicker" name="bpap[scheduled_date]" value="<?php echo $item->post_date; ?>">
    </div>

    <div class="grid-12-12">
        <input type="hidden" name="bpap[author_id]" value="<?php echo bp_loggedin_user_id() ?>" />

        <?php if(isset($_GET['id']) && is_numeric($_GET['id'])){ ?>
            <input type="hidden" name="bpap[ID]" value="<?php echo $item->ID ?>" />            
            <input class="button-primary left" name="bpap[action][notice_update]" type="submit" value="<?php _e('Update Notice', 'bpap'); ?>" />
        <?php }else{ ?>
            <input class="button-primary left" name="bpap[action][notice_create]" type="submit" value="<?php _e('Create Notice', 'bpap'); ?>"/>
        <?php } ?>
    </div>
</fieldset>