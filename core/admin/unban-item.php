<?php
/** @var $item WP_POST*/

$update = false;
if(isset($_GET['id']) && is_numeric($_GET['id']))
    $update = true;
?>
<fieldset>
    <legend><?php _e('Unban Members', 'bpap'); ?></legend>

    <div class="grid-12-12">
        <label class="major"><?php _e('In which group you want to unban member(s)?', 'bpap'); ?> <em class="formee-req">*</em></label>
        <?php
        $groups = bpap_get_groups_with_banned();
        ?>
        <select class="formee-small" name="bpap[group_id]" id="unban_group_id" data-placeholder="<?php _e('Please select a group', 'bpap'); ?>">
            <option value="0"></option>
            <?php
            foreach($groups as $one) {
                echo '<option value="'.$one->group_id.'" '.selected($item->post_parent, $one->group_id).'>'.$one->name.'</option>';
            } ?>
        </select>
        <p class="description"><?php _e('Only those groups, that have banned members, are displayed in a list.'); ?></p>
    </div>

    <!-- Select members -->
    <div class="grid-12-12">
        <label class="major"><?php _e('Members', 'bpap'); ?> <em class="formee-req">*</em></label>
        <?php
        $checked_all = $checked_some = '';
        if ( empty($item) || !$update || $item->post_excerpt == 'all' ) {
            $checked_all = 'checked="checked"';
        }
        if ( isset($item->post_excerpt) && $item->post_excerpt == 'some' ) {
            $checked_some = 'checked="checked"';
        }
        ?>
        <label>
            <input type="radio" name="bpap[group_members]" <?php echo $checked_all ?> value="all" class="unban_members" />
            <?php _e('All banned group members', 'bpap'); ?>
        </label>
        <label>
            <input type="radio" name="bpap[group_members]" <?php echo $checked_some ?> value="some" class="unban_members" />
            <?php _e('Only some banned group members', 'bpap'); ?>
        </label>
        <?php
        // get the list of members of a selected group via ajax

        ?>
        <select id="unban_user_ids" name="bpap[unban_user_ids][]" multiple data-placeholder="<?php _e('Please select users', 'bpap'); ?>" style="display:none">
            <?php
            if($update){
                // get all groups members
                $selected_users = json_decode($item->post_content);
                $members = bpap_get_banned_members_in_group($item->post_parent);
                foreach($members as $member){
                    $selected = '';
                    if(in_array($member->user_id, $selected_users))
                        $selected = 'selected="selected"';
                    echo '<option value="'.$member->user_id.'" '.$selected.'>'.$member->display_name.'</option>';
                }
            }else{ ?>
                <option value=""></option>
            <?php } ?>
            <!-- Members will be inserted here based on a selected group -->
        </select>
        <p class="description"><?php _e('Only banned users are displayed in a list. You can select several users one by one.', 'bpap'); ?></p>
    </div>

    <!-- When to process -->
    <div class="grid-12-12">
        <label class="major"><?php _e('When the action should be done?', 'bpap'); ?> <em class="formee-req">*</em></label>
        <input type="text" class="formee-small datetimepicker" name="bpap[scheduled_date]" value="<?php echo !empty($item) ? $item->post_date : ''; ?>">
        <p class="description clear"><?php _e('The actual action time will vary and depends on number of visitors of your site and the Core Precision settings.', 'bpap'); ?></p>
    </div>

    <div class="grid-12-12">
        <?php if($update){ ?>
            <script type="text/javascript">jQuery('#unban_user_ids').chosen();</script>
            <input type="hidden" name="bpap[ID]" value="<?php echo !empty($item) ? $item->ID : '' ?>" />
            <input class="button-primary left" name="bpap[action][unban_update]" type="submit" value="<?php _e('Update Unban Action', 'bpap'); ?>" />
        <?php }else{ ?>
            <input class="button-primary left" name="bpap[action][unban_create]" type="submit" value="<?php _e('Create Unban Action', 'bpap'); ?>"/>
        <?php } ?>
    </div>
</fieldset>