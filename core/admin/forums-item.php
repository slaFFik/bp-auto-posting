<?php
/** @vat $item WP_POST */
?>

<fieldset>
    <legend><?php
        if(isset($_GET['id']) && is_numeric($_GET['id'])){
            _e('Update The Topic', 'bpap');
        }else{
            _e('Create New Topic', 'bpap');
        }
    ?></legend>

    <!-- Title -->
    <div class="grid-12-12">
        <label class="major"><?php _e('Topic Title', 'bpap'); ?> <em class="formee-req">*</em></label>
        <input type="text" id="topic_title" name="bpap[title]" value="<?php echo !empty($item) ? esc_attr($item->post_title) : ''; ?>">
    </div>

    <script type="text/javascript">jQuery('#topic_title').focus();</script>

    <!-- Slug -->
    <?php if(isset($_GET['id']) && is_numeric($_GET['id'])){ ?>
        <div class="grid-12-12">
            <label class="major"><?php _e('Topic Link', 'bpap'); ?></label>
            <input type="text" disabled="disabled" value="<?php echo !empty($item) ? esc_attr($item->guid) : ''; ?>">
            <p class="description clear"><?php _e('Not editable', 'bpap'); ?></p>
        </div>
    <?php } ?>

    <!-- Content -->
    <div class="grid-12-12">
        <label class="major"><?php _e('Topic Content', 'bpap'); ?> <em class="formee-req">*</em></label>
        <?php wp_editor(
            !empty($item) ? $item->post_content : '', // content text
                'content', // id="content"
                array(
                    'media_buttons' => false,
                    'textarea_name' => 'bpap[content]'
                )); ?>
    </div>

    <!-- Date -->
    <div class="grid-12-12">
        <label class="major"><?php _e('When the topic should be published?', 'bpap'); ?> <em class="formee-req">*</em></label>
        <input type="text" class="formee-small datetimepicker" name="bpap[scheduled_date]" value="<?php echo !empty($item) ? esc_attr($item->post_date) : ''; ?>">
        <p class="description clear"><?php _e('The actual publishing time will vary and depends on number of visitors of your site and the Core Precision settings.', 'bpap'); ?></p>
    </div>

    <!-- Author -->
    <div class="grid-12-12">
        <label class="major"><?php _e('Who will publish this topic?', 'bpap'); ?> <em class="formee-req">*</em></label>
        <?php
        $args = array(
                    'type'            => 'alphabetical',
                    'per_page'        => '10000',
                    'populate_extras' => false
                );
        if ( bp_has_members( $args ) ) : ?>
            <select class="formee-small" name="bpap[author_id]" id="topic_post_author">
                <?php while ( bp_members() ) : bp_the_member(); ?>
                    <option value="<?php bp_member_user_id(); ?>" <?php selected( (!empty($item) ? $item->post_author : ''), bp_get_member_user_id()); ?>><?php bp_member_name() ?></option>
                <?php endwhile; ?>
            </select>
        <?php endif; ?>
        <p class="description clear"><?php _e('You can specify whose name will be displayed as an author of this topic.', 'bpap'); ?></p>
    </div>

    <!-- Group -->
    <div class="grid-12-12">
        <label class="major"><?php _e('In which group forum this topic will be published?', 'bpap'); ?> <em class="formee-req">*</em></label>
        <?php
        $groups = groups_get_groups(array(
                                        'type'            => 'alphabetical',
                                        'show_hidden'     => true,
                                        'per_page'        => 9999,
                                        'populate_extras' => false
                                    ));
        ?>
        <select class="formee-small" name="bpap[group_id]" id="topic_group_id">
            <?php foreach($groups['groups'] as $one) {
                if($one->enable_forum == '1')
                    echo '<option value="'.$one->id.'" '.selected((!empty($item) ? $item->post_parent : ''), $one->id).'>'.$one->name.'</option>';
            } ?>
        </select>
    </div>

    <!-- Notification settings (BuddyPress Activity & emails) -->
    <div class="grid-12-12">
        <?php
        if (empty($item) || !isset($item->post_content_filtered)) {
            $post_content_filtered = '';
        } else {
            $post_content_filtered = $item->post_content_filtered;
        }

        $to_activity = $email_members = $email_admins = 'no';
        $to_ping     = json_decode($post_content_filtered);
        if(isset($to_ping->to_activity))
            $to_activity = $to_ping->to_activity;
        if(isset($to_ping->email_members))
            $email_members = $to_ping->email_members;
        if(isset($to_ping->email_admins))
            $email_admins = $to_ping->email_admins;
        ?>
        <label class="major"><?php _e('Define some notification settings:', 'bpap'); ?></label>
        <label>
            <input type="checkbox" name="bpap[to_activity]" <?php echo checked('yes', $to_activity); ?> value="yes">
            <?php _e('Post a notification to activity feed of a website.', 'bpap'); ?>
        </label>
        <label>
            <input type="checkbox" name="bpap[email_members]" <?php echo checked('yes', $email_members); ?> value="yes">
            <?php _e('Send an email to all group members.', 'bpap'); ?>
        </label>
        <label>
            <input type="checkbox" name="bpap[email_admins]" <?php echo checked('yes', $email_admins); ?> value="yes">
            <?php _e('Send an email to group admins only.', 'bpap'); ?>
        </label>
        <p class="description"><?php _e('All these actions will fire when the topic will be published.', 'bpap'); ?></p>
    </div>

    <!-- Action Buttons -->
    <div class="grid-12-12">
        <?php if(isset($_GET['id']) && is_numeric($_GET['id'])){ ?>
            <input type="hidden" name="bpap[ID]" value="<?php echo !empty($item) ? $item->ID : '' ?>" />
            <input class="button-primary left" name="bpap[action][topic_update]" type="submit" value="<?php _e('Update Topic', 'bpap'); ?>" />
        <?php }else{ ?>
            <input class="button-primary left" name="bpap[action][topic_create]" type="submit" value="<?php _e('Create Topic', 'bpap'); ?>"/>
        <?php } ?>
    </div>
</fieldset>