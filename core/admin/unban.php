<?php
// Prevent error
if(!class_exists('WP_List_Table')){
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class BPAP_Unban_Table extends WP_List_Table{

    public $per_page = 20;

    /**
     * Constructor, we override the parent to pass our own arguments
     * We usually focus on three parameters:
     *     singular and plural labels,
     *         as well as whether the class supports AJAX.
     */
    function __construct() {
        parent::__construct( array(
            'singular' => 'bpap_unban',  // Singular label
            'plural'   => 'bpap_unbans', // Plural label, also this well be one of the table css class
            'ajax'     => false
        ) );
    }

    /**
     * Define the columns that are going to be used in the table
     * @return array $columns, the array of columns to use with the table
     */
    function get_columns() {
        $columns['cb']           = '<input type="checkbox" />';
        $columns['post_content'] = __('Who will be unbanned',  'bpap');
        $columns['post_parent']  = __('Group', 'bpap');
        $columns['post_date']    = __('Date',  'bpap');

        return $columns;
    }

    /**
     * Decide which columns to activate the sorting functionality on
     * @return array $sortable, the array of columns that can be sorted by the user
     */
    public function get_sortable_columns() {
        $sortable = array(
            // 'post_parent' => array('post_parent', false),
            'post_date'   => array('post_date', true)
        );

        return $sortable;
    }

    // Bulk options to selected items
    function get_bulk_actions() {
        $actions = array(
            'plus_1hr' => __('Reshedule: +1 hour', 'bpap'),
            'plus_6hr' => __('Reshedule: +6 hours', 'bpap'),
            'plus_1d'  => __('Reshedule: +1 day', 'bpap'),
            'plus_3d'  => __('Reshedule: +3 days', 'bpap'),
            'plus_1w'  => __('Reshedule: +1 week', 'bpap'),
            'delete'   => __('Delete', 'bpap')
        );
        return $actions;
    }

    function process_bulk_action() {
        if(empty($_POST['bpap_unban_ids']))
            return;

        /** @var $wpdb WPDB */
        global $wpdb;

        // Detect when a bulk action is being triggered...
        switch($this->current_action()){
            case 'delete':
                foreach((array)$_POST['bpap_unban_ids'] as $id){
                    // force remove -  bypass the bin
                    wp_delete_post($id, true);
                }
                break;

            case 'plus_1hr':
                foreach((array)$_POST['bpap_unban_ids'] as $id){
                    // update the post date
                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts}
                                                    SET
                                                        `post_date` = (`post_date` + INTERVAL 1 HOUR),
                                                        `post_date_gmt` = (`post_date_gmt` + INTERVAL 1 HOUR)
                                                    WHERE `ID` = %d", $id));
                }
                break;

            case 'plus_6hr':
                foreach((array)$_POST['bpap_unban_ids'] as $id){
                    // update the post date
                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts}
                                                    SET
                                                        `post_date` = (`post_date` + INTERVAL 6 HOUR),
                                                        `post_date_gmt` = (`post_date_gmt` + INTERVAL 6 HOUR)
                                                    WHERE `ID` = %d", $id));
                }
                break;

            case 'plus_1d':
                foreach((array)$_POST['bpap_unban_ids'] as $id){
                    // update the post date
                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts}
                                                    SET
                                                        `post_date` = (`post_date` + INTERVAL 1 DAY),
                                                        `post_date_gmt` = (`post_date_gmt` + INTERVAL 1 DAY)
                                                    WHERE `ID` = %d", $id));
                }
                break;

            case 'plus_3d':
                foreach((array)$_POST['bpap_unban_ids'] as $id){
                    // update the post date
                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts}
                                                    SET
                                                        `post_date` = (`post_date` + INTERVAL 3 DAY),
                                                        `post_date_gmt` = (`post_date_gmt` + INTERVAL 3 DAY)
                                                    WHERE `ID` = %d", $id));
                }
                break;

            case 'plus_1w':
                foreach((array)$_POST['bpap_unban_ids'] as $id){
                    // update the post date
                    $wpdb->query($wpdb->prepare("UPDATE {$wpdb->posts}
                                                    SET
                                                        `post_date` = (`post_date` + INTERVAL 7 DAY),
                                                        `post_date_gmt` = (`post_date_gmt` + INTERVAL 7 DAY)
                                                    WHERE `ID` = %d", $id));
                }
                break;

            default:
                // nothing to do here
                break;
        }
    }

    /**
     * Prepare the table with different parameters, pagination, columns and table elements
     */
    function prepare_items() {
        /** @var $wpdb WPDB */
        global $wpdb;
        $hidden = array();

        $columns  = $this->get_columns();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

        // Do smth with seleceted bulk rows
        $this->process_bulk_action();

        // Basic query to get them all
        $post_type = BPAP_UNBAN_TYPE;
        $query     = "SELECT * FROM {$wpdb->posts} WHERE `post_type` = '{$post_type}'";

        // Parameters that are going to be used to order the result
        $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'post_date';
        $order   = !empty($_GET["order"])   ? mysql_real_escape_string($_GET["order"])   : 'ASC';
        if(!empty($orderby) & !empty($order)){
            $query .= ' ORDER BY `'.$orderby.'` '.$order;
        }

        /* -- Pagination parameters -- */
        // Number of elements in your table?
        $totalitems = $wpdb->query($query); //return the total number of affected rows

        // Which page is this?
        $paged = $this->get_pagenum();

        // Page Number
        if(empty($paged) || !is_numeric($paged) || $paged <= 0 ){
            $paged = 1;
        }

        // How many pages do we have in total?
        $totalpages = ceil( $totalitems / $this->per_page );

        // adjust the query to take pagination into account
        if(!empty($paged) && !empty($this->per_page)){
            $offset = ($paged - 1) * $this->per_page;
            $query .= ' LIMIT ' . (int) $offset . ',' . (int) $this->per_page;
        }

        // Register the pagination
        $this->set_pagination_args( array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page"    => $this->per_page,
        ) );

        // Fetch the items
        $this->items = $wpdb->get_results($query);
    }

    // Fallback
    function column_default($item, $column_name){
        return '<pre>'.print_r($item,true).'</pre>'; // Show the whole array for troubleshooting purposes
    }

    // Checkboxes
    function column_cb($item){
        return '<input type="checkbox" name="bpap_unban_ids[]" value="'.$item->ID.'" />';
    }

    function column_post_content($item){
        $users_links = array();

        if($item->post_excerpt == 'all') {
            $pre = __('All users will be unbanned', 'bpap');
        } else {
            $users_ids = json_decode($item->post_content);
            foreach($users_ids as $user_id){
                $users_links[] = bp_core_get_userlink($user_id);
            }
            $pre = implode(', ', $users_links);
        }

        if ( empty($pre) ) {
            $pre = __('N/a', 'bpap');
        }

        return $pre . bpap_get_row_item_action($item);
    }

    function column_post_parent($item){
        $link = '';

        if(is_numeric($item->post_parent) && ($item->post_parent > 0)){
            $group = groups_get_group(array('group_id' => $item->post_parent));
            $link = '<a href="'.bp_get_group_permalink($group).'" target="_blank">'.$group->name.'</a>';
        }

        return $link;
    }

    function column_post_date($item){
        return gmdate(get_option('date_format') .' @ '. get_option('time_format'), mysql2date('U', $item->post_date, false));
    }

}