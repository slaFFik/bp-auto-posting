<?php

/**
 * Admin area only and only on required pages
 */
function bpap_load_admin_assets($page){
    add_action('admin_print_styles-'  . $page, 'bpap_admin_styles');
    add_action('admin_print_scripts-' . $page, 'bpap_admin_scripts');
}

/**
 * CSS
 */
function bpap_admin_styles(){
    wp_enqueue_style('bpap_admin_css');
    wp_enqueue_style('bpap_formee');
    wp_enqueue_style('bpap_chosen');
    wp_enqueue_style('bpap_jqueryui');
    wp_enqueue_style('bpap_calendar');
}

/**
 * JavaScript
 */
function bpap_admin_scripts(){
    wp_enqueue_script('bpap_admin_js');
    wp_enqueue_script('bpap_formee');
    wp_enqueue_script('bpap_chosen');
    wp_enqueue_script('bpap_datetimepicker');
    wp_enqueue_script('bpap_calendar');
    wp_enqueue_script('bpap_modernizr');
}

/**
 * Register required admin styles/scripts
 */
add_action( 'admin_init', 'bpap_admin_init' );
function bpap_admin_init(){
    if(!isset($_GET['page']) || $_GET['page'] != 'bpap_admin')
        return false;

    // global
    wp_register_script('bpap_admin_js', BPAP_URL . '/_inc/js/admin-scripts.js', '1.0');
    wp_register_style('bpap_admin_css', BPAP_URL . '/_inc/css/admin-styles.css', false, '1.0');

    // add/edit pages
    if( isset($_GET['action']) && in_array($_GET['action'], array('add', 'edit'))) {
        // js
        wp_register_script('bpap_formee', BPAP_URL . '/_inc/js/formee.js', array('jquery'), '3.1');
        wp_register_script('bpap_chosen', BPAP_URL . '/_inc/js/chosen.jquery.min.js', array('jquery'), '0.9.8');
        wp_register_script('bpap_datetimepicker', BPAP_URL . '/_inc/js/jquery-ui-timepicker-addon.js', array('jquery', 'jquery-ui-datepicker', 'jquery-ui-slider'), '1.0.5');

        // css
        wp_register_style('bpap_formee', BPAP_URL . '/_inc/css/formee.css', false, '3.1');
        wp_register_style('bpap_chosen', BPAP_URL . '/_inc/css/chosen.css', false, '0.9.8');
        wp_register_style('bpap_jqueryui', BPAP_URL . '/_inc/css/jquery-ui.css', false, '1.9');
    }

    // lots of calendar things, so lets do it separately
    if( isset($_GET['tab']) && $_GET['tab'] == 'calendar' ){
        wp_register_script('bpap_calendar', BPAP_URL . '/_inc/js/jquery.calendario.min.js', array('jquery'), '1.0.0');
        wp_register_script('bpap_modernizr', BPAP_URL . '/_inc/js/modernizr.custom.63321.js', false, '2.6.2');
        wp_register_style('bpap_calendar', BPAP_URL . '/_inc/css/calendar.css', false, '1.0.0');
    }
}

/**
 * Front-end area assets
 */
add_action('init', 'bpap_load_public_assets');
function bpap_load_public_assets(){
    if(is_admin())
        return;


}


?>