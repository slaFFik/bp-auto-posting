<?php
/**
 * Get the list of group members for a specific group
 */
add_action('wp_ajax_bpap_ajax_group_members', 'bpap_ajax_get_group_members');
function bpap_ajax_get_group_members(){
    switch($_GET['method']){
        case 'admins':
            break;

        case 'all':
        default:
            $members = bpap_get_banned_members_in_group($_GET['group_id']);
            echo json_encode($members);
            break;
    }
    die;
}

/**
 * Get all events for a Calendar
 */
add_action('wp_ajax_bpap_ajax_calendar', 'bpap_ajax_calendar');
function bpap_ajax_calendar(){
    switch($_GET['method']){
        case 'get_all':
        default:
            global $wpdb;
            // some defaults
            $cal = $data = $raw = array();

            // get all data for all supported types
            $types = implode("', '", array(BPAP_FORUMS_TOPIC_TYPE, BPAP_NOTICE_TYPE, BPAP_UNBAN_TYPE));

            $raw = $wpdb->get_results("SELECT `ID`, `post_author`, `post_date`, `post_title`, `post_parent`, `post_type`, `guid`, `post_content`, `post_excerpt`
                                        FROM {$wpdb->posts}
                                        WHERE `post_status` = 'bpap_pending'
                                        ORDER BY `post_date` ASC");

            // format the structure we need
            foreach($raw as $one){
                $string = $group_link = $date = $time = '';
                $group  = new Stdclass;

                //cur: 2013-01-16 10:40:44
                //req: m-d-Y
                // 0 - all     1 year     2 month    3 day     4 hours     5 mins     6 secs
                preg_match('~([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})~', $one->post_date, $match);

                $date = $match[2] . '-' . $match[3] . '-' . $match[1];
                $time = '@ ' . $match[4] . ':' . $match[5];

                // group data
                if(!empty($one->post_parent) && $one->post_parent > 0){
                    $group      = groups_get_group(array('group_id' => $one->post_parent));
                    $group_link = '<em><a class="last" href="'.bp_get_group_permalink($group).'" title="'.__('Group:','bpap') . ' ' . esc_attr(strip_tags(bp_get_group_description($group))) . '" target="_blank">'.$group->name.'</a></em>';
                }

                if($one->post_type == 'bpap_forums_topic'){
                    // format the string
                    $string = '<span><abbr title="'.__('Forum Topic', 'bpap').'"><code>'.__('FT', 'bpap').'</code></abbr> '.
                                    $time . ' <a href="?page=bpap_admin&tab=f_topics&action=edit&id=' . $one->ID . '" title="'.__('Click to edit', 'bpap').'">' . $one->post_title . '</a>' .
                                    $group_link . '<div class="clear"></div></span>';
                }

                if($one->post_type == 'bpap_notice'){
                    // format string
                    $string = '<span><abbr title="'.__('Sitewide Notice', 'bpap').'"><code>'.__('SN', 'bpap').'</code></abbr> '.
                                    $time . ' <a href="?page=bpap_admin&tab=notices&action=edit&id=' . $one->ID . '" title="'.__('Click to edit', 'bpap').'">' . $one->post_title . '</a><div class="clear"></div></span>';
                }

                if($one->post_type == 'bpap_unban'){
                    $pre = __('N/a', 'bpap');
                    if($one->post_excerpt == 'all'){
                        $pre = __('All users will be unbanned', 'bpap');
                    }else{
                        $users_ids = json_decode($one->post_content);
                        foreach($users_ids as $user_id){
                            $users_links[] = bp_core_get_userlink($user_id);
                        }
                        $text = '<a href="?page=bpap_admin&tab=unban&action=edit&id=' . $one->ID . '" title="'.__('Click to edit', 'bpap').'">' . __('Unbanning:', 'bpap') . '</a> '.
                                '<u>'. implode('', $users_links) . '</u>';
                    }

                    // format string
                    $string = '<span><abbr title="'.__('Unban Members', 'bpap').'"><code>'.__('UM', 'bpap').'</code></abbr> ' . $time .
                                    $text .
                                    $group_link . '<div class="clear"></div></span>';
                }

                // there can be multiple events on the same date - add to current
                if(isset($cal[$date])){
                    $cal[$date] .= $string;
                }else{
                    $cal[$date] = $string;
                }
            }

            // output as json for calendar
            echo json_encode($cal);
            break;
    }

    die;
}