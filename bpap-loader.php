<?php
/*
Plugin Name: BuddyPress Auto Posting
Plugin URI: http://ovirium.com/
Description: Schedule any content to be published anywhere later in your network.
Version: 1.4
Author: slaFFik
Author URI: http://ovirium.com/
*/

/**
 * Some plugin specific globals
 */
define('BPAP_VER',              '1.4');
define('BPAP_PATH',              dirname(__FILE__));
define('BPAP_URL',               plugins_url('', __FILE__));
define('BPAP_FORUMS_TOPIC_TYPE', 'bpap_forums_topic');
define('BPAP_NOTICE_TYPE',       'bpap_notice');
define('BPAP_UNBAN_TYPE',        'bpap_unban');

/**
 * Define default settings on activation
 */
register_activation_hook( __FILE__, 'bpap_activate');
function bpap_activate() {
    $bpap = array();

    // Define default values
    $bpap['cron']            = '300';
    $bpap['deactivate']      = 'save';
    $bpap['notify']['email'] = array();
    $bpap['emails']          = array(
                                    'f_topics' => array(
                                        'subject' => __('Group Forum Topic Published', 'bpap'),
                                        'content' => __('Hello,<br /><br />New topic called <a href=\"%TOPIC_LINK%\">%TOPIC_TITLE%</a> has just been published in the group <a href=\"%GROUP_LINK%\">%GROUP_TITLE%</a>.', 'bpap')
                                    )
                                );

    update_option('bpap', $bpap, '', 'yes');

    // init cron
    bpap_cron_create();
}

/**
 * Remove all the settings on deactivation
 */
register_deactivation_hook( __FILE__, 'bpap_deactivate');
function bpap_deactivate(){
    $bpap = get_option('bpap');

    if(isset($bpap['deactivate']) && $bpap['deactivate'] == 'delete'){
        global $wpdb;

        // remove settings
        delete_option('bpap');

        // delete all crons
        bpap_cron_clear();

        // remove scheduled Forum topics
        $post_types[] = BPAP_FORUMS_TOPIC_TYPE;
        $post_types[] = BPAP_NOTICE_TYPE;
        $post_types[] = BPAP_UNBAN_TYPE;
        $types        = implode(",'", $post_types);
        $wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->posts} WHERE `post_type` IN %s;", $types));
    }
}

/**
 * Load i18n
 */
add_action('plugins_loaded', 'bpap_load_textdomain');
function bpap_load_textdomain() {
    load_plugin_textdomain( 'bpap', false, dirname( plugin_basename( __FILE__ ) ) . '/langs/' );
}

/**
 * Include all the cron functions
 */
require_once(BPAP_PATH . '/core/bpap-cron.php');

/**
 * Main plugin loader - init everything
 */
add_action('bp_init', 'bpap_init');
function bpap_init(){
    // some helpful functions that do different things
    require_once(BPAP_PATH . '/core/bpap-helper.php');

    // load admin area
    if(is_admin() && (is_super_admin() || is_network_admin())){
        require_once(BPAP_PATH . '/core/bpap-admin.php');
    }

    // all css + js files with appropriate logic
    require_once(BPAP_PATH . '/core/bpap-cssjs.php');

    // all ajax related stuff
    require_once(BPAP_PATH . '/core/bpap-ajax.php');

    /**
     * Register forum post type
     */
    register_post_type(BPAP_FORUMS_TOPIC_TYPE,
                        array(
                            'label' => 'BPAP ' . __('Scheduled Forums Topics', 'bpap')
                        ));
    register_post_type(BPAP_NOTICE_TYPE,
                        array(
                            'label' => 'BPAP ' . __('Scheduled Notices', 'bpap')
                        ));
    register_post_type(BPAP_UNBAN_TYPE,
                        array(
                            'label' => 'BPAP ' . __('Scheduled Unban Actions', 'bpap')
                        ));

    /**
     * Register new post status - to prevent any possible problems
     */
    register_post_status('bpap_pending',
                        array(
                            'public'                    => false,
                            'exclude_from_search'       => true,
                            'show_in_admin_all_list'    => false,
                            'show_in_admin_status_list' => false
                        ));

    /**
     * Dirty hack
     */
    $cron_time = false;
    if(isset($_POST['bpap']['action']['settings']['cron'])){
        bpap_cron_clear();
        if(!empty($_POST['bpap']['cron']))
            $cron_time = $_POST['bpap']['cron'];
    }
    bpap_cron_create($cron_time);
}