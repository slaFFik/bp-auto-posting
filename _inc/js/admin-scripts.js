jQuery('#bpap-admin').ready(function(){

    // Bulk selection
    jQuery('#bpap-admin .tablenav select[name="action"]').click(function(){
        if(jQuery(this).attr('checked') == 'checked'){
            jQuery('input[name=bpap_forum_topics_ids[]]').attr('checked', 'checked');
        }else{
            jQuery('input[name=bpap_forum_topics_ids[]]').attr('checked', false);
        }
    });

    /**
     * Date/timepicker on add/edit pages
     */
    var timepicker_options = {
        minDate:    0, // from now to the future
        dateFormat: 'yy-mm-dd',
        timeFormat: 'hh:mm:ss',
        stepMinute: 5
    };
    if(jQuery('.datetimepicker').length)
        jQuery('.datetimepicker').datetimepicker(timepicker_options);
    if(jQuery('.timepicker').length)
        jQuery('.timepicker').timepicker(timepicker_options);

    /**
     * Pretty selectbox options
     */
    if(jQuery('#unban_group_id').length)
        jQuery('#unban_group_id').chosen();
    if(jQuery('#topic_group_id').length)
        jQuery('#topic_group_id').chosen();
    if(jQuery('#topic_post_author').length)
        jQuery('#topic_post_author').chosen();

    /**
     * Ajax functionality
     */
    // Get appripriate members for selected group
    jQuery('#unban_group_id, .unban_members').change(function(){
        var group_id = jQuery('#unban_group_id').val();
        var method   = jQuery('.unban_members:checked').val();
        if(method == 'some' && group_id != '0'){
            // make a request and create chosen
            bpap_init_group_members_select('unban', method, group_id);
        }
        if(method == 'all'){
            bpap_destroy_chosen('#unban_user_ids');
        }
    });
});

function bpap_destroy_chosen(item){
    jQuery(item).removeClass("chzn-done").data("chosen", null);
    jQuery(item+'_chzn').remove();
}

/**
 * Do the request and get select-ready list of users + init the chosen
 */
function bpap_init_group_members_select(page, method, group_id){
    jQuery.ajax({
        type: 'GET',
        url: ajaxurl,
        dataType: 'json',
        data: {
            action:  'bpap_ajax_group_members',
            method:   method,
            group_id: group_id
        },
        success: function(members){
            var select = '';
            var obj = jQuery('#'+page+'_user_ids');
            jQuery.each(members, function(i, member){
                select += '<option value="'+member.user_id+'">'+member.display_name+'</option>';
            });
            obj.html('').append(select);

            // destroy chosen if was initialized
            obj.val('').trigger("liszt:updated");
            // reinit chosen again
            jQuery('#'+page+'_user_ids').chosen();
        }
    });
}