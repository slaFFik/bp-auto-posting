Social activity automation in a box - schedule any content to be published anywhere later in your network.

== Description ==

BuddyPress Auto Posting v1.3 lets site admins to schedule different actions to be fired or some content to be published:
- schedule forum topics in any group
- schedule site-wide notices to be published
- schedule group members unbanning
Everything at a time you specify!

That gives you a unique ability to:
- plan the traffic on your site (with a cool and easy-to-use calendar!)
- promote something at a certain time (using future links of upcoming forum topics)
- notify users about any group events/news on time (very flexible ways to cofigure email behaviour)
- have more control on your site!

And of course you will have really flexible settings management - configure everything like you want.

I'm really open to new suggestions, that's why uservoice idea management is implemented right on into the dashboard (see Feedback page).

This plugin will be developed further and in future releases it will have much more features. Here are only some of them:
- add tags to forum topics
- schedule activity stream updates (appropriate functionality for *every* user on a website - or for admins only)
- schedule personal messages sending (for all users on a site)
- twitter, facebook, instagram, foursquare [you suggest more] integration
- and much more!

= Whom this plugin will be useful for? =

If you:
- value your own time
- have lots of responsibilities
- want to spend less time working and more enjoying
- want to implement awesome functionality into your network
you should definitely buy this plugin!

== Installation ==

After the purchase you will get the zip folder with the plugin folder. There are 2 ways to install the plugin:
1) Use the WordPress plugin uploader in the admin area.
2) Upload the unzipped plugin folder `buddypress-auto-posting` to `wp-content/plugins/` folder on your server.

Now you can activate the plugin and you are ready to start schedule content in your network :)

== Answers to some of your questions ==

= Why I see less options in admin area comparing with screenshots? =

That's ok, don't worry. Each and every BuddyPress install might be different and unique - you can just have some components deactivated. In case the plugin doesn't see required activated BuddyPress component it will just ignore that functionality to prevent any possible errors.
Solution: just activate needed BP components.

= Will this work on WordPress multisite? =

Yes! If your WordPress installation has Multisite mode enabled, BuddyPress Auto Posting will support that too (plugin admin area will be in WordPress Network section).

= Where can I get support or report a bug? Can I suggest new features or vote for implementing something earlier? =

Use built-in Uservoice widget to propose ideas and contact form on Ovirium.com for reporting bugs.

= Does this plugin support translation? =

Yes! Every string can be translated into any language. Unfortunately, currently there are no available other languages (except English) but if you can help with this - this will be really appreciated!

== Changelog ==

v1.4 - 06.09.2014
* various fixes and improvements
* using <code>wp_mail()</code> instead of <code>mail()</code>
* checking compatibility with WordPress 4.0 and BuddyPress 2.0.2

v1.3 - 11.01.2013
* change the author of scheduled topics
* implemented calendar
* better items management right from the list
* some other changes